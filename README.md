# Codebase release 0.5 for HarmonicBalance.jl

by Jan Košata, Javier del Pino, Toni L. Heugel, Oded Zilberberg

SciPost Phys. Codebases 6-r0.5 (2022) - published 2022-08-24

[DOI:10.21468/SciPostPhysCodeb.6-r0.5](https://doi.org/10.21468/SciPostPhysCodeb.6-r0.5)

This repository archives a fixed release as a citable refereed publication. Please refer to the [publication page](https://scipost.org/SciPostPhysCodeb.6-r0.5) to view the full bundle and get information on how to cite it.

For a link to the latest/development version, check the Resources section below.

Copyright Jan Košata, Javier del Pino, Toni L. Heugel, Oded Zilberberg

This README is published under the terms of the CC BY 4.0 license. For the license to the actual codebase, please refer to the license specification in the codebase folder.

## Resources:

* Codebase release version (archive) repository at [https://git.scipost.org/SciPostPhysCodeb/SciPostPhysCodeb.6-r0.5](https://git.scipost.org/SciPostPhysCodeb/SciPostPhysCodeb.6-r0.5)
* Live (external) repository at [https://github.com/NonlinearOscillations/HarmonicBalance.jl/releases/tag/v0.5.2](https://github.com/NonlinearOscillations/HarmonicBalance.jl/releases/tag/v0.5.2)
